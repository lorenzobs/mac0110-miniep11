function palindromo(palavra)
	palavra = tratamento(palavra)
	for i in 1:div(length(palavra),2)
		if palavra[i] != palavra[length(palavra)-i+1]
			return false
		end
	end 
	return true
end

function tratamento(palavra)
	resultado = ""
	palavra =lowercase(palavra)
	for i in palavra
		k = Int64(i)		
		if 97 <= k <=122
			resultado = string(resultado,i)
		elseif k > 122
			if i == 'â' || i == 'á' || i == 'ã' || i == 'à'
				resultado = string(resultado,'a')
			elseif i == 'ô' || i == 'ó' || i == 'õ'
				resultado = string(resultado,'o')
			elseif i == 'ê' || i == 'é'
				resultado = string(resultado,'e')
			elseif i == 'í'
				resultado = string(resultado,'i')
			elseif i == 'ú'
				resultado = string(resultado,'u')
			end
		end
	end
	return resultado
end

using Test

function testapali()
	@test palindromo("O voo do ovo.") == true
	@test palindromo("A rua Laura.") == true
	@test palindromo("O Atari piratão.") == true
	@test palindromo("O romano acata amores a damas amadas e Roma ataca o namoro.") == true
	@test palindromo("Me vê se a panela da moça é de aço, Madalena Paes, e vem.") == true
	@test palindromo("Cenoura é bom") == false
	@test palindromo("Belezura de dia") == false
	@test palindromo("Passarei em MAC0110") == false
	println("Final dos testes")
end
# testapali()

